module register(clk,d,en,q);

parameter WIDTH = 8;

input clk;
input [WIDTH-1:0] d;
input en;
output reg [WIDTH-1:0] q;

always @( posedge clk ) 
  if (en)
    q <= d;
endmodule
