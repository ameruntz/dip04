module ssd(seg,an,clk,disp);
output reg [7:0] seg;
output [3:0] an;
input [31:0] disp;
input clk;

parameter width = 18;

reg [width:0] counter;

wire [1:0] sw = counter[width:width-1];
wire [3:0] an =  ~(4'b0001 << sw);

always @(posedge clk)
  counter <= counter + 1;

always @(*) begin
  case (sw)
    2'b00 : seg = disp[7:0];
    2'b01 : seg = disp[15:8];
    2'b10 : seg = disp[23:16];
    2'b11 : seg = disp[31:24];
    default : seg = 8'hff;
  endcase
end

endmodule
