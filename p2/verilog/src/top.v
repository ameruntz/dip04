module top(seg,an,clk,sw);

output [7:0] seg;
output [3:0] an;
input [7:0] sw;
input clk;

wire [31:0] disp = {4{~sw}};
ssd prikaz(.seg(seg),.an(an),.clk(clk),.disp(disp));

endmodule




